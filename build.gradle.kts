import org.jetbrains.kotlin.gradle.tasks.*

plugins {
	kotlin("jvm") version "1.4.31"
	application
}

val cliktVersion: String by project
val coroutinesVersion: String by project
val freemarkerVersion: String by project
val mordantVersion: String by project
val mockkVersion: String by project
val koinVersion: String by project
val kotestVersion: String by project

group = "io.malteesch"
version = "1.0-SNAPSHOT"

application.mainClass.set("TexMakeKt")

repositories {
	mavenCentral()
	jcenter()
}

dependencies {
	implementation(kotlin("stdlib"))
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
	// Templates
	implementation("org.freemarker:freemarker:$freemarkerVersion")
	// CLI
	implementation("com.github.ajalt.clikt:clikt:$cliktVersion")
	implementation("com.github.ajalt.mordant:mordant:$mordantVersion")
	// Koin
	implementation("org.koin:koin-core:$koinVersion")
	// Testing
	testImplementation("org.koin:koin-test:$koinVersion")
	testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
	testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
	testImplementation("com.google.jimfs:jimfs:1.1")
	testImplementation("io.mockk:mockk:$mockkVersion")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		useIR = true
		jvmTarget = "1.8"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
