package command

import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.*
import java.nio.file.*
import kotlin.io.path.*

class Root : NoOpCliktCommand(name = "texmake", help = "A tool to manage LaTeX projects") {

	private val pwd = Paths.get("").toAbsolutePath()

	private val directory by option("-d", "--dir", help = "The directory of your project")
		.path(canBeFile = false, mustBeWritable = true)
		.default(pwd)

	val workDir: Path
		get() = this.directory.toAbsolutePath()
}
