package command

import org.koin.dsl.*
import kotlin.io.path.*

val commandModule = module {
	single { Root() }
	single { New(get(), get(), get()) }
}
