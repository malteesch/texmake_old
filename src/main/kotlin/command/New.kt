package command

import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.mordant.rendering.TextColors.*
import com.github.ajalt.mordant.terminal.*
import kotlinx.coroutines.*
import project.*

class New(
	private val rootCommand: Root,
	private val projectManager: ProjectManager,
	private val terminal: Terminal
) :
	CliktCommand(name = "new", help = "Create a new project") {

	private val name by option("-n", "--name", help = "The name of the new project")
		.prompt("What should be the name of the project?")

	private val documents by option("-d", "--document", help = "The documents that are managed by the project")
		.multiple()

	override fun run() = runBlocking {
		val directory = rootCommand.workDir
		terminal.println("Creating new project ${brightGreen(name)} in $directory")

		val project = ProjectModel(name, directory.resolve(name), documents)
		projectManager.createProject(project)
	}
}
