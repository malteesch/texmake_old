import com.github.ajalt.clikt.core.*
import com.github.ajalt.mordant.terminal.*
import command.*
import filesystem.*
import org.koin.core.component.*
import org.koin.core.context.*
import org.koin.dsl.*
import project.*
import template.*

@KoinApiExtension
class TexMake : KoinComponent {
	private val rootCommand by inject<Root>()
	private val newCommand by inject<New>()

	fun run(args: Array<String>) =
		rootCommand.subcommands(
			newCommand
		).main(args)
}

@KoinApiExtension
fun main(args: Array<String>) {
	val commonModule = module {
		single { Terminal() }
	}
	startKoin {
		modules(commonModule, commandModule, projectModule, fileSystemModule, templateModule)
	}

	TexMake().run(args)
}
