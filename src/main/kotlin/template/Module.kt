package template

import org.koin.dsl.*

val templateModule = module {
	single { configuration }
	single { TemplateProcessor(get()) }
}
