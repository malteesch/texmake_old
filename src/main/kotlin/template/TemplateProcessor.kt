package template

import freemarker.template.*
import kotlinx.coroutines.*
import java.io.*

class TemplateProcessor(private val templateConfiguration: Configuration) {

	fun processAsync(template: AvailableTemplate, model: Map<String, Any>): Deferred<Reader> =
		GlobalScope.async(Dispatchers.IO) {
			val out = StringWriter()
			templateConfiguration.getTemplate(template.templateName).process(model, out)
			StringReader(out.buffer.fold("") { str, char -> str + char })
		}
}
