package template

import freemarker.template.*
import java.nio.charset.*
import java.nio.file.*

val TEMPLATE_DIRECTORY: Path = Paths.get("templates")

val configuration: Configuration = Configuration(Configuration.VERSION_2_3_31).apply {
	setClassLoaderForTemplateLoading(javaClass.classLoader, TEMPLATE_DIRECTORY.toString())
	defaultEncoding = StandardCharsets.UTF_8.name()
}
