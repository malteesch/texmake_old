package template

enum class AvailableTemplate(val fileName: String) {
	GITIGNORE(".gitignore"),
	GITLABCIYML(".gitlab-ci.yml");

	val templateName
		get() = "$fileName.ftl"
}
