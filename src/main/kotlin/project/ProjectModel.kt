package project

import java.nio.file.*
import kotlin.io.path.*

data class ProjectModel(val name: String, val path: Path, val documents: List<String>)
