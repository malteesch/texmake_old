package project

import org.koin.dsl.*

val projectModule = module {
	single { ProjectManager(get(), get()) }
}
