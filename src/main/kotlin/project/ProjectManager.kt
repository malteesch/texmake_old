package project

import filesystem.*
import kotlinx.coroutines.*
import template.*
import java.nio.file.*
import kotlin.io.path.*

class ProjectManager(
	private val templateProcessor: TemplateProcessor,
	private val fileRepository: FileRepository
) {

	suspend fun createProject(project: ProjectModel) {
		createRootPathAsync(project.path).await()

		val templateModel = mapOf(
			"project" to project
		)

		val filesWithModel = listOf(
			templateModel to AvailableTemplate.GITIGNORE,
			templateModel to AvailableTemplate.GITLABCIYML
		)
		val readers = processRootFilesAsync(filesWithModel).awaitAll()

		filesWithModel
			.zip(readers)
			.map { (fileWithModel, reader) ->
				fileRepository.writeFileAsync(
					project.path,
					fileWithModel.second.fileName,
					reader
				)
			}.awaitAll()

		project.documents
			.map { documentName ->
				listOf(
					fileRepository.writeFileAsync(project.path.resolve("src").resolve(documentName), ".gitkeep"),
					fileRepository.writeFileAsync(project.path.resolve("out").resolve(documentName), ".gitkeep"),
					fileRepository.writeFileAsync(project.path.resolve("src"), "$documentName.tex")
				)
			}
			.flatten()
			.awaitAll()
	}

	private fun createRootPathAsync(path: Path) = GlobalScope.async(Dispatchers.IO) {
		Files.createDirectory(path)
	}

	private fun processRootFilesAsync(files: Collection<Pair<Map<String, Any>, AvailableTemplate>>) =
		files.map { (model, template) ->
			templateProcessor.processAsync(template, model)
		}
}
