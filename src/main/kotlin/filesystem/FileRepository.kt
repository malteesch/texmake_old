package filesystem

import kotlinx.coroutines.*
import java.io.*
import java.nio.file.*

class FileRepository {

	fun writeFileAsync(
		path: Path,
		fileName: String,
		content: Reader = StringReader("")
	): Deferred<Path?> {
		val filePath = path.resolve(fileName)
		return GlobalScope.async(Dispatchers.IO) {
			Files.createDirectories(path)
			Files.write(filePath, content.readLines(), StandardOpenOption.CREATE)
		}
	}

	fun readFileAsync(path: Path): Deferred<Reader?> {
		return GlobalScope.async(Dispatchers.IO) {
			if (Files.exists(path)) {
				Files.newBufferedReader(path)
			} else null
		}
	}
}
