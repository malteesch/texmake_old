package filesystem

import org.koin.dsl.*
import kotlin.io.path.*

val fileSystemModule = module {
	single { FileRepository() }
}
