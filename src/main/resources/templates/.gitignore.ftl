<#-- @ftlvariable name="project" type="project.ProjectModel" -->
!/out/.gitkeep
<#list project.documents as document>
!/out/${document}/.gitkeep
</#list>
/*.pdf
/.idea/
/out/*
