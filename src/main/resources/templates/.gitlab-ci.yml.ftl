<#-- @ftlvariable name="project" type="project.ProjectModel" -->
image: blang/latex:ctanfull

stages:
  - documents
<#list project.documents as document>

${document}:
  stage: documents
  needs: []
  script:
    - latexmk -pdf -outdir=../out -cd src/${document}.tex
    - mv out/${document}.pdf ${document}.pdf
  artifacts:
    paths:
      - ./*.pdf
  rules:
    - changes:
      - .gitlab-ci.yml
      - src/${document}.tex
      - src/main.bib
      - src/${document}/**/*
      - src/assets/**/*
</#list>
