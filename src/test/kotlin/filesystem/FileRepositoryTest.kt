package filesystem

import com.google.common.jimfs.*
import io.kotest.core.spec.style.*
import io.kotest.matchers.*
import io.kotest.matchers.nulls.*
import io.kotest.matchers.paths.*
import java.nio.file.*

class FileRepositoryTest : FunSpec({

	context("#writeFileAsync") {

		test("writeFileAsync with pwd returns new file") {
			val subject = FileRepository()
			Jimfs.newFileSystem(Configuration.unix()).use { fs ->
				val pwd = fs.getPath("")
				val newFile = subject.writeFileAsync(pwd, "newFile").await()

				newFile?.shouldExist()
				newFile?.fileName.toString() shouldBe "newFile"
			}
		}

		test("writeFileAsync with existing subdirectory returns new file") {
			val subject = FileRepository()
			Jimfs.newFileSystem(Configuration.unix()).use { fs ->
				val pwd = fs.getPath("")
				val subDirectory = Files.createDirectories(pwd.resolve("existingDir"))
				val newFile = subject.writeFileAsync(subDirectory, "newFile").await()

				newFile?.shouldExist()
				newFile?.fileName.toString() shouldBe "newFile"
			}
		}

		test("writeFileAsync with non existing directory returns new file") {
			val subject = FileRepository()
			Jimfs.newFileSystem(Configuration.unix()).use { fs ->
				val pwd = fs.getPath("")
				val newFile = subject.writeFileAsync(pwd.resolve("nonExisting"), "newFile").await()

				newFile?.shouldExist()
				newFile?.shouldNotBeNull()
			}
		}
	}

	context("#readFileAsync") {

		test("readFileAsync with existing file returns file contents") {
			val subject = FileRepository()
			Jimfs.newFileSystem(Configuration.unix()).use { fs ->
				val pwd = fs.getPath("")
				val filePath = subject.writeFileAsync(pwd, "newFile", "content").await()

				val reader = subject.readFileAsync(filePath!!).await()
				reader.use { br ->
					br?.readLine() shouldBe "content"
				}
			}
		}

		test("readFileAsync with non existing file returns null") {
			val subject = FileRepository()
			Jimfs.newFileSystem(Configuration.unix()).use { fs ->
				val pwd = fs.getPath("")
				val filePath = pwd.resolve("nonExistingFile")

				val content = subject.readFileAsync(filePath).await()
				content.shouldBeNull()
			}
		}
	}
})
