import io.kotest.core.config.*
import io.kotest.core.test.*

@Suppress("unused") // scanned for by kotest
object ProjectConfig : AbstractProjectConfig() {
	override val testCaseOrder: TestCaseOrder get() = TestCaseOrder.Random
	override val parallelism: Int get() = 4
}
